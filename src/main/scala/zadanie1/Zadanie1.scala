package zadanie1

import java.io.{File, FileInputStream}

import com.google.gson.Gson
import org.apache.poi.ss.usermodel.{Cell, CellType, Row}
import org.apache.poi.xssf.usermodel.XSSFWorkbook

object Zadanie1 extends App {

  def getCellValue(cell: Cell): Any = {
    cell.getCellType match {
      case CellType.STRING => cell.getStringCellValue
      case CellType.NUMERIC => cell.getNumericCellValue
      case CellType.BLANK => None
      case _ => throw new RuntimeException(" this error occured when reading ")
    }
  }

  def getRowValues(row: Row): Seq[Any] = {
    val cells: Seq[Cell] = for (i <- 0 until row.getPhysicalNumberOfCells) yield row.getCell(i)
    val rowValues = cells.map(x => getCellValue(x))
    rowValues
  }

  def convert(seq: Seq[Any]): List[Node] = {
    val leaves = seq.map {
      case Vector(a: Int, b: String, 3) => Node(a, b, List.empty[Node])
      case _ => ()
    }.filterNot(_.equals()).toList.asInstanceOf[List[Node]]

    val secondLevel = seq.map {
      case Vector(a: Int, b: String, 2) =>
        if (leaves.exists(x => x.id == a + 1)) {
          if (leaves.exists(x => x.id == a + 2))
            if (leaves.exists(x => x.id == a + 3))
              throw new RuntimeException
          else
            Node(a, b, leaves.filter(x => x.id == a + 1 || x.id == a + 2).filterNot(_.equals()))
          else Node(a, b, leaves.filter(x => x.id == a + 1).filterNot(_.equals()))
        }
        else Node(a, b, List.empty[Node])
      case _ => ()
    }.filterNot(_.equals()).toList.asInstanceOf[List[Node]]

    val lastLevel = seq.map {
      case Vector(a: Int, b: String, 1) =>
        if (secondLevel.exists(x => x.id == a + 1)) {
          val numOfNodes = secondLevel.filter(x => x.id == a + 1).filterNot(_.equals()).last.nodes.length
          if (secondLevel.exists(x => x.id == a + 2 + numOfNodes))
            Node(a, b, secondLevel.filter(x => x.id == a + 1 || x.id == a + 2 + numOfNodes).filterNot(_.equals()))
          else Node(a, b, secondLevel.filter(x => x.id == a + 1).filterNot(_.equals()))
        }
        else Node(a, b, List.empty[Node])
      case _ => ()
    }.filterNot(_.equals()).toList.asInstanceOf[List[Node]]
    lastLevel
  }

  def readXlxs(pathname: String) = {
    val f = new File(pathname)
    val workbook = new XSSFWorkbook(new FileInputStream(f))
    val sheet = workbook.getSheetAt(0)
    workbook.close()
    val et: Seq[Node] = {
      for (i <- 0 until sheet.getPhysicalNumberOfRows) yield sheet.getRow(i)
    }
      .map(x => getRowValues(x))
      .map { case Vector(a, b, c, d) =>
        if (a != None && b == None && c == None) Vector(d.asInstanceOf[Double].toInt, a, 1)
        else if (a == None && b != None && c == None) Vector(d.asInstanceOf[Double].toInt, b, 2)
        else if (a == None && b == None && c != None) Vector(d.asInstanceOf[Double].toInt, c, 3)
      }.filterNot(_.equals()).asInstanceOf[Seq[Node]]
    convert(et)
  }

  def listToJson(x: List[Node]) ={
    val gson = new Gson
    gson.toJson(x.toArray.map {
      x: Node =>
        JsonNode(x.id, x.name, x.nodes.toArray.map {
          x: Node =>
            JsonNode(x.id, x.name, x.nodes.toArray.map {
              x: Node => JsonNode(x.id, x.name, Array.empty[JsonNode])
            })
        })
    })
  }

  println(listToJson(readXlxs("Scala_zadanie.xlsx")))

}
