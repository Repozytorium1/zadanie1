package zadanie1

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.must.Matchers

class Zadanie1Test extends AnyFlatSpec with Matchers {

  "readXlsx" should "return List() for test_empty" in {
    Zadanie1.readXlxs("Scala_zadanie_test_empty.xlsx") mustBe List()
  }

  "readXlsx" should "return throw exception for test_AA3" in {
    assertThrows[RuntimeException] {
      Zadanie1.readXlxs("Scala_zadanie_test_AA3.xlsx")
    }
  }

  "readXlsx" should "return List od Nodes for test_3" in {
      Zadanie1.readXlxs("Scala_zadanie_test_3.xlsx") mustBe List(Node(1,"A",List(Node(2,"AA",List(Node(3,"AA1",List()), Node(4,"AA2",List()))), Node(5,"AB",List(Node(6,"AB1",List()), Node(7,"AB2",List()))))), Node(8,"B",List(Node(9,"BA",List()), Node(10,"BB",List()))), Node(11,"C",List()))
  }

  "listToJson" should "return [] for test_empty" in {
    Zadanie1.listToJson(Zadanie1.readXlxs("Scala_zadanie_test_empty.xlsx")) mustBe "[]"
  }

  "listToJson" should "return Json of Nodes for test_3" in {
    Zadanie1.listToJson(Zadanie1.readXlxs("Scala_zadanie_test_3.xlsx")) mustBe "[{\"id\":1,\"name\":\"A\",\"nodes\":[{\"id\":2,\"name\":\"AA\",\"nodes\":[{\"id\":3,\"name\":\"AA1\",\"nodes\":[]},{\"id\":4,\"name\":\"AA2\",\"nodes\":[]}]},{\"id\":5,\"name\":\"AB\",\"nodes\":[{\"id\":6,\"name\":\"AB1\",\"nodes\":[]},{\"id\":7,\"name\":\"AB2\",\"nodes\":[]}]}]},{\"id\":8,\"name\":\"B\",\"nodes\":[{\"id\":9,\"name\":\"BA\",\"nodes\":[]},{\"id\":10,\"name\":\"BB\",\"nodes\":[]}]},{\"id\":11,\"name\":\"C\",\"nodes\":[]}]"
  }

}
